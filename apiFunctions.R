library(jsonlite)
library(rlist)
library(httr)
library(yaml)
library(dplyr)
library(readr)


# yml <- read_yaml(secrets)
# httr::set_config(config(ssl_verifypeer = FALSE))
# srvNameYml = paste0("acny-edw", srv)



secrets = "~/secrets.yaml"
srv = "dev"

yml <- read_yaml(secrets)
httr::set_config(config(ssl_verifypeer = FALSE))
srvNameYml = paste0("acny-edw", srv)


DataMartsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts"),
                    authenticate(yml[[srvNameYml]]$user, 
                                 yml[[srvNameYml]]$password, 
                                 "ntlm")
)
DataMartsCallData = content(DataMartsCall)
dataMarts = DataMartsCallData$value

getDataMarts = function(srv='dev'){
  DataMartsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts"),
                      authenticate(yml[[srvNameYml]]$user,
                                   yml[[srvNameYml]]$password,
                                   "ntlm")
  )
  DataMartsCallData = content(DataMartsCall)
  dataMarts = DataMartsCallData$value
  return(dataMarts)
}

getDataMartID = function(samName,secrets  = secrets , srv = srv ){
  dataMart = list.filter(dataMarts, Name == samName)
  return(dataMart[[1]]$Id)
}


getEntities <- function(dataMartID){
  DataMartEntitiesCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities"),
                      authenticate(yml[[srvNameYml]]$user, 
                                   yml[[srvNameYml]]$password, 
                                   "ntlm")
  )
  DataMartEntitiesData = content(DataMartEntitiesCall)
  DataMartEntities = DataMartEntitiesData$value
  return(DataMartEntities)
}



getBindings <- function(dataMartID, entityID){
  BindingCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",entityID,")/SourceBindings"),
                             authenticate(yml[[srvNameYml]]$user, 
                                          yml[[srvNameYml]]$password, 
                                          "ntlm")
  )
  BindingData = content(BindingCall)
  Bindings = BindingData$value
  return(Bindings)
}


getSummary <- function(samName){
  Call = GET(paste0("https://acny-atlasdev.acny.hosted/MetadataService/v1/SummaryBindings"),
             authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, "ntlm"))
  
  SummaryBindings = content(Call)$value
  SummaryBindings = list.filter(SummaryBindings, DataMartName == samName)
  list.map(SummaryBindings,c(Id,BindingName,DestinationEntityId,DestinationEntityName,DataMartId,DataMartName,SourceConnectionId,Classification))%>%
    as.data.frame(row.names=c('Id','BindingName','DestinationEntityId','DestinationEntityName','DataMartId','DataMartName','SourceConnectionId','Classification'),
                  col.names=c(1:length(SummaryBindings)))%>%
    t()
  return(list.map(SummaryBindings,c(Id,BindingName,DestinationEntityId,DestinationEntityName,DataMartId,DataMartName,SourceConnectionId,Classification))%>%
           as.data.frame(row.names=c('Id','BindingName','DestinationEntityId','DestinationEntityName','DataMartId','DataMartName','SourceConnectionId','Classification'),
                         col.names=c(1:length(SummaryBindings)))%>%
           t())%>%
    as.data.frame()
  }




writeBinding <- function(EntityID, dataMartID, ConnectionId, BindingName, SQL, classification = 'Rule', BindingType="SQL"){
  # Classification must assume one of the following values: Event, Rule, Population, Metric, Summary, ReportingView, Generic, SourceMart
  
  SQL <- paste(read_lines(file = SQL),collapse = '\\n ')
  SQL <- iconv(SQL, "UTF-8", "UTF-8",sub='')
  SQL <- gsub(x = SQL, pattern = "\t", replacement ='')
  
  
  BindingJSON = sprintf('
{
"DestinationEntityId": %s,
"DataMartId": %s,
"SourceConnectionId": %s,
"Name": "%s",
"Classification": "%s",
"BindingType": "%s",
"LoadTypeCode": "Full",
"AttributeValues": [
  {
    "AttributeName": "UserDefinedSQL",
    "AttributeValue": "%s"
  }
]
}
', EntityID, dataMartID, ConnectionId, BindingName, classification, BindingType, SQL)
  
  createNewBinding = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/SourceBindings"),
                          authenticate(yml[[srvNameYml]]$user,  yml[[srvNameYml]]$password,   "ntlm"),
                         body = jsonlite::fromJSON(BindingJSON),  encode = 'json'
  )
  print(toJSON(content(createNewBinding),pretty = T)) # content
  newBindingID = content(createNewBinding)$Id 
  
  return(newBindingID)
}

# bcall = writeBinding(EntityID=13387,
#                      dataMartID=1428,
#                      ConnectionId=1449,
#                      BindingName="newclass",
#                      classification = "Metric",
#                      SQL="select patientID,ClaimID, BindingID as bid from samcdphp.npotemp.dx")

writeRBinding <- function(EntityID, dataMartID, ConnectionId, BindingName, Rscript, fields, rDependencies,DfToOutput, sourceEntityID, SourceDfName, classification = 'Rule', BindingType="R",srv='dev'){
  # Classification must assume one of the following values: Event, Rule, Population, Metric, Summary, ReportingView, Generic, SourceMart
  
  Rscript <- paste(read_lines(file = Rscript),collapse = '\\n ')
  Rscript <- iconv(Rscript, "UTF-8", "UTF-8",sub='')
  Rscript <- gsub(x = Rscript, pattern = "\t", replacement ='')
  
  rDependencies <- paste(read_lines(file = rDependencies),collapse = '\\n ')
  rDependencies <- iconv(rDependencies, "UTF-8", "UTF-8",sub='')
  rDependencies <- gsub(x = rDependencies, pattern = "\t", replacement ='')
  
  BindingJSON = sprintf('
{
"DestinationEntityId": %s,
"DataMartId": %s,
"SourceConnectionId": %s,
"Name": "%s",
"Classification": "%s",
"BindingType": "%s",
"LoadTypeCode": "Full",
"AttributeValues": [
  {
    "AttributeName": "Script",
    "AttributeValue": "%s"
  },
  {
    "AttributeName": "ScriptDependencies",
    "AttributeValue": "%s"
  },
  {
    "AttributeName": "ExecutionEnvironmentID",
    "AttributeValue": "1"
  },
  {
    "AttributeName": "ExecutionEnvironmentName",
    "AttributeValue": "R-3.5.1"
  },
  {
    "AttributeName": "ResultDataFrameName",
    "AttributeValue": "%s"
  }
]
}
', EntityID, dataMartID, ConnectionId, BindingName, classification, BindingType, Rscript,rDependencies, DfToOutput)
  
  createNewBinding = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/SourceBindings"),
                          authenticate(yml[[srvNameYml]]$user,  yml[[srvNameYml]]$password,   "ntlm"),
                          body = jsonlite::fromJSON(BindingJSON),  encode = 'json'
  )
  print(toJSON(content(createNewBinding),pretty = T)) # content
  newBindingID = content(createNewBinding)$Id 
  
  
  BindingJSON = sprintf('
      "BindingId": "%s",
      "SourceEntityId": "%s",
      "BlocksLoad": false,
      "SourceAliasName": "%s"
', newBindingID, sourceEntityID, SourceDfName)
  
  sourceEntity = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/SourceBindings(",newBindingID,")/SourcedByEntities"),
                          authenticate(yml[[srvNameYml]]$user,  yml[[srvNameYml]]$password,   "ntlm"),
                          body = jsonlite::fromJSON(BindingJSON),  encode = 'json'
  )
  print(toJSON(content(sourceEntity),pretty = T)) # content
  sourceEntityContent = content(sourceEntity) 
  
  response = regenDestEntity(dataMartID = dataMartID, 
                             EntityID = EntityID, 
                             SqlOrCsvFile = ifelse(is.null(fieldTypesPath), sqlScriptPath, fieldTypesPath)
  ) # fix success
  print(content(response))
  
  return(newBindingID)
}


writeRtoSAM <- function(samName, bindingName, RScriptPath ,secrets  = '~/secrets.yaml' , srv = 'dev', runBinding=FALSE, fieldTypesPath = NULL, warningOverride = FALSE, RunDownstream='false'){
  library(readr)
  if(!warningOverride){
    if(tolower(readline(prompt=sprintf("Are you sure you want to overwrite the %s binding? y or n: ",bindingName))) != 'y') 
    {return("Canceled")}
  }
  
  Rscript <- read_file(RScriptPath)
  Rscript <- iconv(Rscript, "UTF-8", "UTF-8",sub='')
  
  Sql <- paste(read_lines(file = RScriptPath), collapse = '\n ')
  Sql = gsub(pattern = '\t',replacement = '',Sql)
  
  
  yml <- read_yaml(secrets)
  srvNameYml = paste0("acny-edw", srv)
  DataMartsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts"),
                      authenticate(yml[[srvNameYml]]$user, 
                                   yml[[srvNameYml]]$password, 
                                   "ntlm")
  )
  
  DataMartsCallData = content(DataMartsCall)
  dataMarts = DataMartsCallData$value
  
  # TODO: consider adding a grep function for name similarity 
  dataMart = list.filter(dataMarts, Name == samName)
  # TODO: add check for uniqueness
  
  sourceBindingCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/SourceBindings"),
                          authenticate(yml[[srvNameYml]]$user, 
                                       yml[[srvNameYml]]$password, 
                                       "ntlm")
  )
  
  sourceBindingData = content(sourceBindingCall)
  SourceBindings = sourceBindingData$value
  
  codedSB = list.filter(SourceBindings, DataMartId==dataMart[[1]]$Id)
  
  binding = list.filter(codedSB, Name==bindingName)
  
  DataMartId=dataMart[[1]]$Id
  bindingID = binding[[1]]$Id
  EntityId = binding[[1]]$DestinationEntityId
  UserDefinedSQL = list.filter(binding[[1]]$AttributeValues, AttributeName == 'Script')
  
  
  sourceBindingAttributesPATCH = PATCH(paste0("https://acny-atlas", 
                                              srv, 
                                              ".acny.hosted/MetadataService/v1/DataMarts(",
                                              binding[[1]]$DataMartId,
                                              ")/Entities(",
                                              binding[[1]]$DestinationEntityId,
                                              ")/SourceBindings(",
                                              binding[[1]]$Id,
                                              ")/AttributeValues(",
                                              UserDefinedSQL[[1]]$Id,
                                              ")"),
                                       authenticate(yml[[srvNameYml]]$user, 
                                                    yml[[srvNameYml]]$password, 
                                                    "ntlm"), 
                                       body = list(AttributeName='Script',AttributeValue=Rscript),
                                       encode='json'
  )
  
  if(sourceBindingAttributesPATCH$status_code!=204) {
    print(content(sourceBindingAttributesPATCH))
    return(sourceBindingAttributesPATCH)
  }
  
  response = regenDestEntity(dataMartID = DataMartId, 
                             EntityID = EntityId, 
                             SqlOrCsvFile = ifelse(is.null(fieldTypesPath), sqlScriptPath, fieldTypesPath)
  ) # fix success
  
  if(!runBinding) return(response)
  
  
  response = runBinding(DataMartId = DataMartId, EntityId = EntityId, BindingId = bindingID,RunDownstream=RunDownstream,srv=srv)
  
  if(F) return(response)
  
  return("Sucess")
  
}


deleteBinding <- function(dataMartID,EntityID,bindingID){
  
  deleteBindingCall = DELETE(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/SourceBindings(",bindingID,")"),
                           authenticate(yml[[srvNameYml]]$user,  yml[[srvNameYml]]$password, 'ntlm')  )
  return(deleteBindingCall)
}



getEntityFields <- function(dataMartID, EntityID){
  EntityFieldsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields"),
                             authenticate(yml[[srvNameYml]]$user, 
                                          yml[[srvNameYml]]$password, 
                                          "ntlm")
  )
  EntityFieldsData = content(EntityFieldsCall)
  EntityFields = EntityFieldsData$value
  
  return(EntityFields)
}

writeEntityFieldFilesFromSAM <- function(samName, destinationFolder){
  dir.create(destinationFolder, showWarnings = F,recursive=T)
  bindings = getSummary(samName)
  for ( row in 1:nrow(bindings))  {
    EntityFields = getEntityFields(bindings$DataMartId[row],bindings$DestinationEntityId[row])
    list.map(EntityFields,c(FieldName,DataType,IsPrimaryKey,IsNullable,Ordinal,Status))%>%
      as.data.frame(row.names=c('FieldName','DataType','IsPrimaryKey','IsNullable','Ordinal','Status'),
                    col.names=c(1:length(EntityFields)))%>%
      t()%>%
      write.csv(file = paste0(destinationFolder,bindings$DestinationEntityName[row],'.csv'),row.names = F)
    
    }
 return('Complete') 
}

writeField <- function(dataMartID,EntityID, FieldNM, DataType='varchar(255)', pk='false',nullable = 'true'){
  FieldsJSON = sprintf('{
  "FieldName": "%s",
  "DataType": "%s",
  "IsPrimaryKey": %s,
  "IsNullable": %s
}', FieldNM, DataType, pk, nullable)
  
  newFieldCall = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields"),
                         authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, 'ntlm'),
                         body = jsonlite::fromJSON(FieldsJSON),  encode = 'json'
  )
  return(newFieldCall)
  
}


# SqlOrCsvFile
regenDestEntity <- function(dataMartID,EntityID,SqlOrCsvFile){
  # TODO: there is an issue with csv files sometimes being true for in the if clause trying to determine if it is a sql file
  if(grepl('*.csv', SqlOrCsvFile, ignore.case = T)){
    FieldsJSON = read.csv(SqlOrCsvFile)%>%filter(Ordinal>3)
  }

  # get fields
  fields = getEntityFields(dataMartID = dataMartID, EntityID = EntityID)
  
  # delete fields
  
  if (length(fields)!=0) for (i in 1:length(fields)){
    if (fields[[i]]$FieldName %in% c('LastLoadDTS','BindingNM','BindingID')) next
    deleteFieldCall = DELETE(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields(",fields[[i]]$Id,")"),
                        authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, 'ntlm')
    )
    if(deleteFieldCall$status_code != 204) return(paste("Delete error",content(deleteFieldCall)))
  }
  
  # write new fields
  for (i in 1:nrow(FieldsJSON)){
    print(i)
    json = FieldsJSON[i,]%>% toJSON(dataframe = "rows",pretty=T,auto_unbox = T)
    print(json)
    newFieldCall = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields"),
                        authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, 'ntlm'),
                        body = jsonlite::fromJSON(gsub(pattern = '\\[|\\]',replacement = '',x = json)) ,  
                        encode = 'json'
                        )
    if(newFieldCall$status_code != 201) {
      msg = paste("post error",content(newFieldCall))
      return(msg)
    }
  }

  return("regenDestEntity success")  
    
}

uploadFieldInfo <- function(CsvFile,dataMartID,EntityID){
  
  fields = getEntityFields(dataMartID = dataMartID, EntityID = EntityID)
  
  # delete fields
  
  if (length(fields)!=0) for (i in 1:length(fields)){
    if (fields[[i]]$FieldName %in% c('LastLoadDTS','BindingNM','BindingID')) next
    deleteFieldCall = DELETE(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields(",fields[[i]]$Id,")"),
                             authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, 'ntlm')
    )
    if(deleteFieldCall$status_code != 204) return(paste("Delete error",content(deleteFieldCall)))
  }
  
  
  FieldsJSON = read.csv(CsvFile)%>%filter(Ordinal>3)
  for (i in 1:nrow(FieldsJSON)){
    print(i)
    json = FieldsJSON[i,]%>% toJSON(dataframe = "rows",pretty=T,auto_unbox = T)
    print(json)
    newFieldCall = POST(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")/Fields"),
                        authenticate(yml[[srvNameYml]]$user, yml[[srvNameYml]]$password, 'ntlm'),
                        body = jsonlite::fromJSON(gsub(pattern = '\\[|\\]',replacement = '',x = json)) ,  
                        encode = 'json'
    )
    if(newFieldCall$status_code != 201) {
      msg = paste("post error",content(newFieldCall))
      return(msg)
    }
  }
  
}

createDataMart <- function(dataMartName,SamDatabaseName,SamSchemaName){
  Entityjson = sprintf('
 {
      "Name": "%s",
      "DataMartType": "Subject Area",
      "Version": "1.0",
      "AttributeValues": [
      {
        "AttributeName": "DestinationObjectPrefixCode",
        "AttributeValue": "%s"
      },
      {
        "AttributeName": "SamDatabaseName",
        "AttributeValue": "%s"
      },
      {
        "AttributeName": "SamSchemaName",
        "AttributeValue": "%s"
      },
      {
        "AttributeName": "SamTypeCode",
        "AttributeValue": "Clinical"
      },
      {
        "AttributeName": "StatusCode",
        "AttributeValue": "Active"
      },
      {
        "AttributeName": "SuiteName",
        "AttributeValue": "%s"
      }
    ]
}', dataMartName, dataMartName,SamDatabaseName,SamSchemaName, dataMartName)
  
  createNewDataMart = POST(paste0("https://acny-atlas", 
                                srv, 
                                ".acny.hosted/MetadataService/v1/DataMarts"),
                         authenticate(yml[[srvNameYml]]$user, 
                                      yml[[srvNameYml]]$password, 
                                      'ntlm'),
                         body = jsonlite::fromJSON(Entityjson),  encode = 'json'
  )
  print(toJSON(content(createNewDataMart),pretty = T)) # content
  DataMart = content(createNewDataMart)
  dataMartID = DataMart$Id
  
  return(dataMartID)
}

connectDataMart <- function(dataMartID){
  Entityjson = sprintf('
 {
  "SystemName": "EDW",
  "Description": "Default EDW Connection",
  "SystemVendorName": "Health Catalyst",
  "SystemVersion": "2020",
  "DataMartId": "%s",
  "AttributeValues": [
    {
      "AttributeName": "IsSystem",
      "AttributeValue": "True"
    }
  ]
  }', dataMartID)
  
  newConnection = POST(paste0("https://acny-atlas", 
                                  srv, 
                                  ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Connections"),
                           authenticate(yml[[srvNameYml]]$user, 
                                        yml[[srvNameYml]]$password, 
                                        'ntlm'),
                           body = jsonlite::fromJSON(Entityjson),  encode = 'json'
  )
  print(toJSON(content(newConnection),pretty = T)) # content
  connection = content(newConnection)
  connectionID = connection$Id 
  
  return(connectionID)
}


deleteEntity <- function(dataMartID,EntityID){

  deleteNewEntity = DELETE(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts(",dataMartID,")/Entities(",EntityID,")"),
                         authenticate(yml[[srvNameYml]]$user, 
                                      yml[[srvNameYml]]$password, 
                                      'ntlm')  )
  return(deleteNewEntity)
}

# deleteNewEntity = deleteEntity(dataMartID = 1428,EntityID = 13386)  

# TODO: write connectionID function  


createEntity <- function(dataMartID, DatabaseName,SchemaName,EntityName,ConnectionId){
    Entityjson = sprintf('
  {
    "DataMartId": %s,
    "ConnectionId": "%s",
    "EntityName": "%s",
    "PersistenceType": "Database",
    "AttributeValues": [
      {
        "AttributeName": "DatabaseName",
        "AttributeValue": "%s"
      },
      {
        "AttributeName": "PersistedFlag",
        "AttributeValue": "1"
      },
      {
        "AttributeName": "SchemaName",
        "AttributeValue": "%s"
      },
      {
        "AttributeName": "TableName",
        "AttributeValue": "%sBASE"
      },
      {
        "AttributeName": "ViewName",
        "AttributeValue": "%s"
      }
    ]
  }', dataMartID,ConnectionId, EntityName,DatabaseName,SchemaName,EntityName,EntityName)

    newEntity = POST(paste0("https://acny-atlas", 
                                              srv, 
                                              ".acny.hosted/MetadataService/v1/DataMarts(",
                                              dataMartID,
                                              ")/Entities"),
                                       authenticate(yml[[srvNameYml]]$user, 
                                                    yml[[srvNameYml]]$password, 
                                                    'ntlm'),
                                      body = jsonlite::fromJSON(Entityjson),  encode = 'json'
                                      )
  print(toJSON(content(newEntity),pretty = T)) # content
  newEntityID = content(newEntity)$Id 
  
  return(newEntityID)
}



getSourceBindings = function(secrets  = secrets , srv = srv ){
  sourceBindingCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/SourceBindings"),
                          authenticate(yml[[srvNameYml]]$user, 
                                       yml[[srvNameYml]]$password, 
                                       "ntlm")
  )
  sourceBindingData = content(sourceBindingCall)
  SourceBindings = sourceBindingData$value
  return(SourceBindings)
}


getSQLFromSAM <- function(samName, outputDir = '~/SAMs/',secrets  = '~/secrets.yaml' , srv = 'dev' ){
  SourceBindings = getSourceBindings(secrets=secrets, srv=srv)
  dataMarts = getDataMarts(srv=srv)
  dataMart = list.filter(dataMarts, Name == samName)
  
  
  codedSB = list.filter(SourceBindings, DataMartId==dataMart[[1]]$Id)
  outputDirFullPath = paste0(outputDir,samName,'/',srv)
  cat('Output to: ', outputDirFullPath,'\n')
  dir.create(outputDirFullPath, showWarnings = F,recursive=T)

  for ( i in codedSB) {
    print(i$Name)
    if (i$BindingType=='SQL') {
      UserDefinedSQL = list.filter(i$AttributeValues, AttributeName == 'UserDefinedSQL')
      code = gsub(pattern = '\r\n',replacement = '\n',x = UserDefinedSQL[[1]]$AttributeValue)
      fileFullPath = paste0(outputDirFullPath,'/', i$Name,'.sql')
    }
    if (i$BindingType=='R') {
      script = list.filter(i$AttributeValues, AttributeName == 'Script')
      code = gsub(pattern = '\r\n',replacement = '\n',x = script[[1]]$AttributeValue)
      fileFullPath = paste0(outputDirFullPath,'/', i$Name,'.R')
      
      ScriptDependencies = list.filter(i$AttributeValues, AttributeName == 'ScriptDependencies')
      dependencies = gsub(pattern = '\r\n',replacement = '\n',x = ScriptDependencies[[1]]$AttributeValue)
      dependenciesFileFullPath = paste0(outputDirFullPath,'/', i$Name,'_dependencies.txt')
      fileConn <- file(dependenciesFileFullPath)
      writeLines(dependencies, fileConn,sep=' \n')
      close(fileConn)
      
    }
    fileConn <- file(fileFullPath)
      writeLines(code, fileConn,sep=' \n')
      close(fileConn)
    
    
  }
  writeEntityFieldFilesFromSAM(samName = samName, destinationFolder = paste(outputDirFullPath,'EntityFields/',sep='/'))

}





findSamByBindingId <- function(bindingId,srv='dev'){
  dataMarts = getDataMarts(srv=srv)
  SourceBindings = getSourceBindings(secrets  = secrets , srv = srv)
  
  codedSB = list.filter(SourceBindings, Id==bindingId)
  
  dataMart = list.filter(dataMarts, Id == codedSB[[1]]$DataMartId)
  
  return (dataMart[[1]]$Name)
}
  



writeSQLtoSAM <- function(samName, bindingName, sqlScriptPath ,secrets  = '~/secrets.yaml' , srv = 'dev', runBinding=FALSE, fieldTypesPath = NULL, warningOverride = FALSE, RunDownstream='false'){
  library(readr)
  if(!warningOverride){
    if(tolower(readline(prompt=sprintf("Are you sure you want to overwrite the %s binding? y or n: ",bindingName))) != 'y') 
    {return("Canceled")}
  }
  
  SQL <- read_file(sqlScriptPath)
  SQL <- iconv(SQL, "UTF-8", "UTF-8",sub='')
  
  Sql <- paste(read_lines(file = sqlScriptPath), collapse = '\n ')
  Sql = gsub(pattern = '\t',replacement = '',Sql)
  
  
  yml <- read_yaml(secrets)
  srvNameYml = paste0("acny-edw", srv)
  DataMartsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts"),
                      authenticate(yml[[srvNameYml]]$user, 
                                   yml[[srvNameYml]]$password, 
                                   "ntlm")
  )
  
  DataMartsCallData = content(DataMartsCall)
  dataMarts = DataMartsCallData$value
  
  # TODO: consider adding a grep function for name similarity 
  dataMart = list.filter(dataMarts, Name == samName)
  # TODO: add check for uniqueness
  
  sourceBindingCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/SourceBindings"),
                          authenticate(yml[[srvNameYml]]$user, 
                                       yml[[srvNameYml]]$password, 
                                       "ntlm")
  )
  
  sourceBindingData = content(sourceBindingCall)
  SourceBindings = sourceBindingData$value
  
  codedSB = list.filter(SourceBindings, DataMartId==dataMart[[1]]$Id)
  
  binding = list.filter(codedSB, Name==bindingName)
  
  DataMartId=dataMart[[1]]$Id
  bindingID = binding[[1]]$Id
  EntityId = binding[[1]]$DestinationEntityId
  UserDefinedSQL = list.filter(binding[[1]]$AttributeValues, AttributeName == 'UserDefinedSQL')
  
  
  sourceBindingAttributesPATCH = PATCH(paste0("https://acny-atlas", 
                                              srv, 
                                              ".acny.hosted/MetadataService/v1/DataMarts(",
                                              binding[[1]]$DataMartId,
                                              ")/Entities(",
                                              binding[[1]]$DestinationEntityId,
                                              ")/SourceBindings(",
                                              binding[[1]]$Id,
                                              ")/AttributeValues(",
                                              UserDefinedSQL[[1]]$Id,
                                              ")"),
                                       authenticate(yml[[srvNameYml]]$user, 
                                                    yml[[srvNameYml]]$password, 
                                                    "ntlm"), 
                                       body = list(AttributeName='UserDefinedSQL',AttributeValue=SQL),
                                       encode='json'
  )
  
  if(sourceBindingAttributesPATCH$status_code!=204) {
    print(content(sourceBindingAttributesPATCH))
    return(sourceBindingAttributesPATCH)
    }
  
  response = regenDestEntity(dataMartID = DataMartId, 
                             EntityID = EntityId, 
                             SqlOrCsvFile = ifelse(is.null(fieldTypesPath), sqlScriptPath, fieldTypesPath)
                             ) # fix success
  
  if(!runBinding) return(response)
  
  
  response = runBinding(DataMartId = DataMartId, EntityId = EntityId, BindingId = bindingID,RunDownstream=RunDownstream,srv=srv)
  
  if(F) return(response)
  
  return("Sucess")
  
}



## upload files from a folder in a loop - this can be improved to do less api calls
refreshAllSQL <- function(SAM_NAME, folder, fieldTypesDirectory=NULL){
  
  if (is.null(fieldTypesDirectory)) fieldTypesDirectory = paste0(folder,'EntityFields/')

  if(tolower(readline(prompt=sprintf("Are you sure you want to overwrite ALL of the bindings in %s? y or n: ", SAM_NAME))) != 'y') 
  {return("Canceled")}
  
  scripts = list.files(folder)
  scripts = scripts[grepl(pattern = '*.sql',x = scripts,ignore.case = T)]
  
  for (script in scripts){
    bindingName = gsub(pattern = '.sql',replacement = '', x = script)
    print(bindingName)
    sqlScriptPath = paste0(folder, script)
    fieldTypesPath = paste0(fieldTypesDirectory, bindingName,'.csv')
    # print(sqlScriptPath)
    
    
    post = writeSQLtoSAM(samName = SAM_NAME,
      bindingName = bindingName,
      sqlScriptPath = sqlScriptPath, runBinding=FALSE, fieldTypesPath = fieldTypesPath, warningOverride=TRUE)
    
    if (!grepl(pattern = 'success',x = post)){
      print('_________________')
      print('Failure')
      print(post)
      print(bindingName)
      print(sqlScriptPath)
      print('_________________')
    }
      
  }
  
}




listSourceMarts <- function(secrets  = secrets , srv = srv ){
  DataMartsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/MetadataService/v1/DataMarts"),
                      authenticate(yml[[srvNameYml]]$user, 
                                   yml[[srvNameYml]]$password, 
                                   "ntlm")
  )
  
  DataMartsCallData = content(DataMartsCall)
  dataMarts = DataMartsCallData$value
  
  # TODO: consider adding a grep function for name similarity 
  sourceDataMart = list.filter(dataMarts, DataMartType == 'Source')
  sourceSystemDataMart = list.filter(dataMarts, DataMartType == 'Source System')
  
  sourceMartNames = c()
  sourceMartIds = c()
  for (i in sourceDataMart) {
    sourceMartIds=c(sourceMartIds, i$Id)
    sourceMartNames=c(sourceMartNames, i$Name )
  }
  
  sourceMarts = data.frame(sourceMartNames, sourceMartIds)
  
  return(sourceMarts)
}




#### processing
getBatches = function(srv='dev'){
  BatchDefinitionsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/DataProcessingService/v1/BatchDefinitions"),
                      authenticate(yml[[srvNameYml]]$user,
                                   yml[[srvNameYml]]$password,
                                   "ntlm")
  )
  BatchDefinitionsData = content(BatchDefinitionsCall)
  BatchDefinitions = BatchDefinitionsData$value
  return(BatchDefinitions)
}

getBatchInfo = function(BatchId, srv='dev'){
  BatchDefinitionsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/DataProcessingService/v1/BatchDefinitions(",BatchId,")"),
                             authenticate(yml[[srvNameYml]]$user,
                                          yml[[srvNameYml]]$password,
                                          "ntlm")
  )
  BatchDefinitionsData = content(BatchDefinitionsCall)
  return(BatchDefinitionsData)
}

getJobInfo = function(JobId, srv='dev'){
  BatchDefinitionsCall = GET(paste0("https://acny-atlas", srv, ".acny.hosted/DataProcessingService/v1/BatchExecutions(",JobId,")"),
                             authenticate(yml[[srvNameYml]]$user,
                                          yml[[srvNameYml]]$password,
                                          "ntlm")
  )
  BatchDefinitionsData = content(BatchDefinitionsCall)
  return(BatchDefinitionsData)
}



getBatchDefinitionId = function(batchName,srv='dev'){
  # TODO: consider adding a grep function for name similarity 
  batches = getBatches()
  batch = list.filter(batches, Name == batchName)
  return(batch[[1]]$Id)
}

runBinding <- function(DataMartId,EntityId, BindingId, RunDownstream='false',srv='dev'){
  batchId = createCustomBindingBatch(DataMartId, EntityId, BindingId, RunDownstream = RunDownstream,srv=srv)
  print(paste('batchID',batchId))
  executeBatchResponse = executeBatch(batchId,srv=srv)
  # Sys.sleep(10)
  # deleteBatchResponse = deleteBatch(batchId)
  return('Running binding')
}


createCustomBindingBatch <-  function(DataMartId,EntityId, BindingId, RunDownstream='false',srv='dev'){
  BatchName = paste('API run custom binding',Sys.time())
  Batchjson = sprintf('{
    "DataMartId": %s,
    "Name": "%s",
    "PipelineType": "Batch",
    "LoadType": "Custom Bindings",
    "IncludeDownstreamDependents": %s,
    "CustomEntities": [
      {
        "EntityId": %s
      }
    ],
    "CustomBindings": [
      {
        "BindingId": %s
      }
    ]  
  }
', DataMartId, BatchName, RunDownstream, EntityId, BindingId)
  
  
  CustomBindingBatch = POST(paste0("https://acny-atlas", 
                                srv, 
                                ".acny.hosted/DataProcessingService/v1/BatchDefinitions"),
                         authenticate(yml[[srvNameYml]]$user, 
                                      yml[[srvNameYml]]$password, 
                                      'ntlm'),
                         body = jsonlite::fromJSON(Batchjson),  encode = 'json'
  )
  CustomBindingBatchData = content(CustomBindingBatch)
  CustomBindingBatchID = CustomBindingBatchData$Id
  
  return(CustomBindingBatchID)
}

# binding = createCustomBindingBatch(DataMartId = 1428, EntityId = 13375, BindingId = 8475)  
  
executeBatch <-  function(BatchId, srv){  
  executeBatchjson = sprintf('{
    "BatchDefinitionId": %s,
    "Status": "Queued"
  }', BatchId)
  
  executeBatch = POST(paste0("https://acny-atlas", 
                                   srv, 
                                   ".acny.hosted/DataProcessingService/v1/BatchExecutions"),
                            authenticate(yml[[srvNameYml]]$user, 
                                         yml[[srvNameYml]]$password, 
                                         'ntlm'),
                            body = jsonlite::fromJSON(executeBatchjson),  encode = 'json'
  )
  executeBatchData = content(executeBatch)
  executeBatchID = executeBatchData$Id
  
  return(executeBatchID)
}



deleteBatch <- function(BatchId){  
  deleteBatchCall = DELETE(paste0("https://acny-atlas", srv, ".acny.hosted/DataProcessingService/v1/BatchDefinitions(",BatchId,")"),
                             authenticate(yml[[srvNameYml]]$user,  yml[[srvNameYml]]$password, 'ntlm')  )
  return(deleteBatchCall)
}



runDataMart <- function(DataMartId,srv='dev'){
  batchId = createDataMartBatch(DataMartId,srv = srv)
  print(paste('batchID',batchId))
  executeBatchResponse = executeBatch(batchId,srv = srv)
  # Sys.sleep(10)
  # deleteBatchResponse = deleteBatch(batchId)
  return('Running binding')
}


createDataMartBatch <-  function(DataMartId, srv='dev'){
  BatchName = paste('API run data mart',Sys.time())
  Batchjson = sprintf('{
    "DataMartId": %s,
    "Name": "%s",
    "PipelineType": "Batch"
  }
', DataMartId, BatchName)
  
  
  CustomDataMartBatch = POST(paste0("https://acny-atlas", 
                                   srv, 
                                   ".acny.hosted/DataProcessingService/v1/BatchDefinitions"),
                            authenticate(yml[[srvNameYml]]$user, 
                                         yml[[srvNameYml]]$password, 
                                         'ntlm'),
                            body = jsonlite::fromJSON(Batchjson),  encode = 'json'
  )
  CustomDataMartBatchData = content(CustomDataMartBatch)
  CustomDataMartBatchID = CustomDataMartBatchData$Id
  
  return(CustomDataMartBatchID)
}


getFieldsFromSQL <- function(folder){
  library(DBI)
  library(odbc)
  
  files = list.files(path = folder)
  for (file in files){
    if(grepl('*.sql', file, ignore.case = T)){
      
      Sql <- paste(read_lines(file = paste0(folder,'/',file)), collapse = '\n ')
      Sql = gsub(pattern = '\t',replacement = '',Sql)
      Sql= gsub(x = Sql,pattern = "'",replacement = "''")
      con <- dbConnect(odbc::odbc(),
                       dsn="acny-edwprod",
                       uid = Sys.getenv("edw_user"),
                       pwd = Sys.getenv("edw_pass"))
      
      insights=dbGetQuery(con, sprintf("DECLARE @query nvarchar(max) = '%s';
      EXEC sp_describe_first_result_set @query, null, 0;
      ",Sql))
      dbDisconnect(con)
  
      Fields = insights%>%
        select(FieldName = name,DataType = system_type_name)%>%
        mutate(IsPrimaryKey = 'false',
               IsNullable = 'true',
               Ordinal = 4:(n()+3))
      
      dir.create(paste0(folder,'/EntityFields/'), showWarnings = F,recursive=T)
      write.csv(Fields,paste0(folder,'/EntityFields/',gsub(pattern = '.sql',replacement = '.csv', x = file, ignore.case = T)),row.names = F)
    }
  }
}


