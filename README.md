# SAM_InteractR

R functions to interact with SAMs

[swagger API MetadataService](https://acny-atlasdev.acny.hosted/MetadataService/swagger/ui/index)

[swagger API webservices](https://acny-atlasdev.acny.hosted/DataProcessingService/swagger/ui/index)

The API allows for get, post and patch commands. When building these functions I would start with get commands, before writing a post command.

# Functions:

## getSQLFromSAM
Downloads all the sql from the bindings of a SAM to a specified folder. 

Arguments
- samName		>> Name of SAM (spelled precicely)	  
- outputDir 	>> folder where sql files will be saved, defaults to '~/SAMs/samName/srv'
- secrets 		>> location of secrets.yaml file defaults to "~/secrets.yaml"
- srv 			>> server you wish to connect to choose "dev" or "prod", defaults to "dev"

## writeSQLtoSAM 
Will write SQL code for an existing binding and updates the entity information. Also has the options to run the binding with or without its dependencies (i.e. all the bindings that use it) (dev only). 

Arguments
- samName		>> Name of SAM (spelled precicely)	  
- bindingName 	>> Name of binding (spelled precicely) 
- sqlScriptPath >> path to sql file to upload to binding 
- secrets 		>> location of secrets.yaml file defaults to "~/secrets.yaml"
- srv 			>> server you wish to connect to choose "dev" or "prod", defaults to "dev" and only works in dev currently


## findSamByBindingId 
Identifies a SAM from the binding id. This is useful if you want to find the SAM that a table come from.

Arguments
- bindingID		>> bindingID, from table	  
- secrets 		>> location of secrets.yaml file defaults to "~/secrets.yaml"
- srv 			>> server you wish to connect to choose "dev" or "prod", defaults to "dev"


## listSoureMarts
Lists the names of all sourcemarts. This is more of an example than a useful function.

Arguments


## getDataMarts 
Returns an R list of all datamarts and their attributes.

Arguments

## getDataMartID = function(samName,secrets  = secrets , srv = srv ){
Returns the datamart id. 

Arguments
- samName - Spelled precicely 

## getEntities 
Returns an R list of all entities and there attributes in a data mart.

Arguments
- dataMartID

## getBindings 
Returns an R list of all bindings and their attributes of an entity.

Arguments
- dataMartID
- entityID

## getSummary 
Returns a dataframe with the names and id values of the bindings and entities in a datamart (SAM) as wellas the id value for the datamart and the connection

Arguments
- samName (spelled precicely)

## writeBinding 
Writes the sql code attribute of a binding. (This is not a great function name, should be depricated and rewriten, but it is also a low level function so NBD?)

Arguments
- EntityID
- dataMartID
- ConnectionId
- BindingName
- SQL sql query
- classification  "Event", "Rule", "Population", "Metric", "Summary", "ReportingView", "Generic", "SourceMart". Defaults to "Event"

## deleteBinding 
Deletes a binding from a data mart (SAM), returns API delete call information.

Arguments
- dataMartID
- EntityID
- bindingID

## getEntityFields 
Returns an R list of all fields and their attributes of an entity.

Arguments
- dataMartID
- EntityID

## writeEntityFieldFilesFromSAM
Writes a csv file, for each entity in the sam, where each file contains field information:
'FieldName','DataType','IsPrimaryKey','IsNullable','Ordinal','Status'

Arguments
- samName
- destinationFolder

## writeField 
writes a single field to an entity. 

Arguments
dataMartID,EntityID, FieldNM

## regenDestEntity 
Writes field information to an entity

Arguments
- dataMartID
- EntityID
- SqlOrCsvFile - currently only accepts a csv file

## uploadFieldInfo 
Writes field information to an entity

Arguments
- CsvFile
- dataMartID
- EntityID

## createDataMart 
Creates a new Subject Area mart. Returns post call infortmation. This can most improved by adding functionality to create source marts as well, most likely. It is also hard coding SAM as an Active and Clinical, so room for improvement there as well.

Arguments
- dataMartName
- SamDatabaseName
- SamSchemaName

## connectDataMart 
Creates a connection to a data mart.Returns post call infortmation. This should be incorporated into the createDataMartFunction.

Arguments
- dataMartID

## deleteEntity 
Deletes an entity from a data mart. Returns delete call infortmation.

Arguments
- dataMartID
- EntityID

## createEntity 
Creates an entity in a datamart. Returns post call infortmation.

Arguments
- dataMartID
- DatabaseName
- SchemaName
- EntityName
- ConnectionId

## getSourceBindings 
Returns an R list of all bindings and their attributes. This is a little slow and is used in other functions. It should be updated to include a datamart and entity id to improve performance speeds.

Arguments

## getSQLFromSAM 
Writes a folder, with the name of the SAM, containing sql files for each sql binding in the sam (named with the binding name, in a folder named dev or prod as applicable).

Arguments
- samName, 
- outputDir  directory '~/SAMs/', 
- srv 	server name, 'dev' or 'prod' defaults to 'dev' 


## findSamByBindingId 
Identifies a SAM from the binding id. This is useful if you want to find the SAM that a table come from.

Arguments
- bindingID		>> bindingID, from table	  
- secrets 		>> location of secrets.yaml file defaults to "~/secrets.yaml"
- srv 			>> server you wish to connect to choose "dev" or "prod", defaults to "dev"

## writeSQLtoSAM 
Will write SQL code for an existing binding and updates the entity information. Also has the options to run the binding with or without its dependencies (i.e. all the bindings that use it) (dev only). 

Arguments
- samName		Name of SAM (spelled precicely)	  
- bindingName 	Name of binding (spelled precicely) 
- sqlScriptPath path to sql file to upload to binding 
- fieldTypesPath path to csv file containing field information of 'FieldName', 'DataType', 'IsPrimaryKey', 'IsNullable', 'Ordinal', 'Status'
- runBinding	option to run the binding, TRUE or FALSE, defaults to FALSE
- RunDownstream will run all bindings that use this binding, 'true' or 'false', defaults to 'false'

## refreshAllSQL
reloads all sql scripts from a directory 

Arguments
- SAM_NAME  Name of SAM (spelled precicely)
- folder    Path to folder containing sql scripts
- fieldTypesDirectory path to folder containing field information

## listSourceMarts 
lists all sourcemarts

Arguments

## getBatches 
lists all batches 

Arguments


## getBatchDefinitionId 
Returns batch id

Arguments
- batchName

## runBinding 
Runs a binding, and optionally its downstream dependents.

Arguments
- DataMartId
- EntityId
- BindingId
- RunDownstream  will run all bindings that use this binding, 'true' or 'false', defaults to 'false'

## createCustomBindingBatch 
Creates a batch and returns an ID

Arguments
- DataMartId
- EntityId
- BindingId
- RunDownstream  option to include downstream entities in batch, 'true' or 'false', defaults to 'false'

## executeBatch 
Runs a batch

Arguments
- BatchId

## deleteBatch 
delets a batch

Arguments
- BatchId

## runDataMart 
Runs a data mart

Arguments
- DataMartId

## createDataMartBatch 
Creates a batch to run a datamart

Arguments
DataMartId

## getFieldsFromSQL
Creates Entity field information for each sql file and writes it to a csv file of the same name in a folder called EntityFields

Arguments
- folder
